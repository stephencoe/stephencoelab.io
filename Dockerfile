FROM node:alpine

MAINTAINER stephen@madebybox.co.uk

#
# Usage
# docker build -t yarn .
# docker run -v $(pwd):/app yarn CMD
#


# Install curl
RUN apk add --update curl && \
    rm -rf /var/cache/apk/*

# Install Yarn
RUN npm install -g --progress=false yarn

WORKDIR /app

VOLUME /app

ENTRYPOINT ["yarn"]

CMD ["--version"]
