import gulp from 'gulp';
import sass from './sass';
import lint from './lint';
import copy from './copy';
import clean from './clean';
import metalsmith from './metalsmith';

/**
 * Build the static templates
 * Compile the sass
 * @return {gulp}
 */
export default function build() {
  return gulp
    .series([
      gulp.parallel([clean, lint()]),
      gulp.parallel([metalsmith, sass]),
      copy,
    ]);
}
