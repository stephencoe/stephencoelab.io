import gulp from 'gulp';

export default function Resume(done) {
  gulp
    .src(`${__dirname}/../src/resume.pug`)
    // .pipe(pug())
    .pipe(gulp.dest(`${__dirname}/../public/resume/index.html`));
  done();
}
