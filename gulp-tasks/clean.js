import del from 'del';

export default function clean(done) {
  del([`${__dirname}/../public`]).then(() => done());
}
