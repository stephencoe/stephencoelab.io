import fs from 'fs';
import Metalsmith from 'metalsmith';
import collections from 'metalsmith-collections';
import excerpts from 'metalsmith-excerpts';
import markdown from 'metalsmith-markdown';
import layouts from 'metalsmith-layouts';
import shortcodes from 'metalsmith-flexible-shortcodes';
import permalinks from 'metalsmith-permalinks';

export default function MetalsmithBuild(done) {
  Metalsmith(__dirname)
    .ignore(['**/content/drafts/**'])
    .metadata({
      sitename: 'Stephen Coe',
      siteurl: 'http://example.com/',
      description: 'Nothing to see here<br>only general ramblings mostly a playground for things.',
      generatorname: 'Metalsmith',
      generatorurl: 'http://metalsmith.io/',
      ga: 'UA-62778995-1',
    })
    .source(`${__dirname}/../content`)
    .destination(`${__dirname}/../public`)
    .clean(true)
    .use(collections({
      posts: {
        pattern: 'posts/**',
        sortBy: 'publishDate',
        reverse: true,
      },
    }))
    .use(shortcodes({
      clean: true,
      parserOpts: {
        openPattern: '\\{{',
        closePattern: '\\}}',
      },
      shortcodes: {
        flickr: (str, params) => `<img src="${params.src}" />`,
        // get the contents of the gist and embed static
        gist: (str, params) => {
          const content = `${params.file} foo bar`;
          return `<div class="code-snippet lang-${params.lang}">
                <pre>${content}</pre>
                <div class="code-snippet__footer">
                  <a class="code-snippet__link icon-github" target="_blank" rel="nofollow" href="https://gist.githubusercontent.com/stephencoe/${params.hash}/raw/${params.file}">View Gist</a>
                </div>
            </div>`;
        },
        inline: (str, params) => {
          const file = fs.readFileSync(`${__dirname}/../src/demo/${params.name}`, 'UTF-8');
          return `<pre class="code-snippet" data-name="${params.name}">${file}</pre>`;
        },
      },
    }))
    .use(markdown())
    .use(permalinks({
      relative: false,
    }))
    .use(layouts({
      engine: 'pug',
      directory: `${__dirname}/../src/layout`,
    }))
    .use(excerpts())
    .build((err) => {
      if (err) throw err;
      console.log('Build successful');
      done();
    });
}
