import gulp from 'gulp';
import eslint from 'gulp-eslint';

export default function JsLint() {
  const directories = [
    `${__dirname}/../**/*.js`,
    // ignore node_modules
    `!${__dirname}/../node_modules/**`,
  ];

  return gulp
    .src(directories)
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
}
