import gulp from 'gulp';
import jsLint from './js-lint';
import sassLint from './sass-lint';

export default function Lint() {
  return gulp.parallel([jsLint, sassLint]);
}
