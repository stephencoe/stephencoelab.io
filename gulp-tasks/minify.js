import gulp from 'gulp';
import minify from 'gulp-csso';

export default function Minify(done) {
  gulp
    .src(`${__dirname}/../public/css/main.css`)
    .pipe(minify())
    .pipe(gulp.dest(`${__dirname}/../public/css`));
  done();
}
