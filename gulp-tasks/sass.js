import gulp from 'gulp';
import sass from 'gulp-sass';

export default function Sass() {
  const options = {
    outputStyle: 'expanded',
  };

  return gulp.src(`${__dirname}/../src/sass/**/*.scss`)
    .pipe(sass(options).on('error', sass.logError))
    .pipe(gulp.dest(`${__dirname}/../public/css`));
}
