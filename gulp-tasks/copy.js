import gulp from 'gulp';

export default function CopyFiles() {
  // const sourceFiles = [
  //   `${__dirname}/../src/fonts`, `${__dirname}/../src/demo`
  // ];
  // const destination = `${__dirname}/../public/fonts`;

  gulp
    .src(`${__dirname}/../src/fonts/**/*`)
    .pipe(gulp.dest(`${__dirname}/../public/fonts`));

  return gulp
    .src(`${__dirname}/../src/demo/**/*`)
    .pipe(gulp.dest(`${__dirname}/../public/demo`));
}
