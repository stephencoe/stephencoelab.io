import gulp from 'gulp';
import sassLint from 'gulp-sass-lint';

export default function SassLint() {
  const options = {
    configFile: `${__dirname}/../.sass-lint.yml`,
  };

  return gulp
    .src(`${__dirname}/../src/sass/**/*.s+(a|c)ss`)
    .pipe(sassLint(options))
    .pipe(sassLint.format())
    .pipe(sassLint.failOnError());
}
