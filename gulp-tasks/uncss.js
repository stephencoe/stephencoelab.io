import gulp from 'gulp';
import uncss from 'gulp-uncss';

export default function Uncss() {
  const options = {
    html: [
      `${__dirname}/../public/**/*.html`,
    ],
  };

  return gulp
    .src(`${__dirname}/../public/css/main.css`)
    .pipe(uncss(options))
    .pipe(gulp.dest(`${__dirname}/../public/css`));
}
