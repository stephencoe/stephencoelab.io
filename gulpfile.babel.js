import gulp from 'gulp';
import browserSync from 'browser-sync';

// tasks
import uncss from './gulp-tasks/uncss';
import minify from './gulp-tasks/minify';
import lint from './gulp-tasks/lint';
import clean from './gulp-tasks/clean';
import build from './gulp-tasks/build';

const BrowserSync = function BrowserSync(done) {
  browserSync.create();
  const options = {
    server: {
      baseDir: './public',
    },
  };
  browserSync.init(options);
  done();
};

const Watch = function Watch() {
  const directories = [
    `${__dirname}/content/**/*.md`,
    `${__dirname}/src/**/*`,
  ];

  gulp
    .watch(directories)
    .on('change',
      gulp.series([
        build(),
        () => browserSync.reload(),
      ]));
};

// Static server
gulp.task(
  'serve',
  gulp.series([build(), BrowserSync, Watch]),
);

gulp.task(
  'build',
  gulp.series([clean, build(), uncss, minify]),
);

gulp.task('test', lint());
