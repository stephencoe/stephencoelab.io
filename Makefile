.PHONY: build

test_build:
	gitlab-ci-multi-runner exec docker \
	--docker-volumes /var/run/docker.sock:/var/run/docker.sock \
	--docker-volumes ${PWD}:/app pages

build: node_modules
	npm run build

node_modules: package.json
	npm install

publish:
	docker build -t stephencoe/yarn .
	docker tag $(docker images | awk '$1 ~ /stephencoe\/yarn/ { print $3}') stephencoe/yarn:latest
	docker push stephencoe/yarn

terraform-config:
	cd infrastructure && \
	terraform remote config \
	-backend=s3 \
	-backend-config="bucket=terraform-state.madebybox.co.uk" \
	-backend-config="key=stephen.madebybox.co.uk/terraform.tfstate" \
	-backend-config="region=eu-west-1"
