variable "domain_name" {}
variable "gitlab_url" {}

#
# Subdomains
# https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains
#
resource "digitalocean_record" "blog" {
  domain = "${digitalocean_domain.domain_name}"
  type = "CNAME"
  name = "blog"
  value = "${gitlab_url}"
}
