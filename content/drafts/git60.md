# Git in 60 seconds



`git init`

# Add some files
`touch .gitignore 1 a b c`

```
git add 1
git commit -m "one love"
echo "just a file" > 1 
git add a
```

# add some text
```
echo "test text" > a 
git commit -am "modified 1, updated a"
echo "test text2" > b 
echo "test text3" > c
touch d
rm 1
git commit -am "no more one"
git add .
```
# oops, d isn't ready
```
git reset d

git commit -m "adding b and c"

echo "test text3 appended" >> c
```
# Hmm, I prefer c how it was before
`git checkout c`


# Actually, all that text I added in the last commit was complete bolony
```
git reset --soft HEAD~1
echo "c should look like this" > c
```
```
git mv a sensitive.txt
git commit -am "rename a"
git rm sensitive.txt && echo sensitive.txt > .gitignore

git commit -am "delete sensitive file"
# Remove the password from history. (https://help.github.com/articles/removing-sensitive-data-from-a-repository/)
git filter-branch --force --index-filter \
'git rm --cached --ignore-unmatch sensitive.txt' \
--prune-empty --tag-name-filter cat -- --all
```

`git statsu`

# Oops, spelt wrong. Maybe enable autocorrect
# Edit the ~/.gitconfig

```
[help]

  # Automatically correct and execute mistyped commands
  autocorrect = 1
```

`git commit -am "fixing c"`

# Now we are ready for d
```
echo "some other text for d" > d
git add d 
git commit -m "add me smoe d"
```



# Damn, im an idiot and spelt it wrong
`git commit -m "add me some d" --amend`
# check the changes
`git log`
# 
# Now when did we delete c
`git log -1 -- 1`

# damn, I commited a password in a
# 

# all these commits are just noise
`git rebase -i `

# Actually, everything we just did was just a test, lets revert the bad things
`git reset --hard HEAD~1`

# 