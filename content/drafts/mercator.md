// https://github.com/tuupola/php_google_maps/blob/master/Google/Maps/Mercator.php

function debounce(func, wait, immediate) {
  var timeout;
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) {
        func.apply(context, args);
      }
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) {
      func.apply(context, args);
    }
  };
}

function Map(container, bounds) {
  this.bounds = {};
  this.grid = {};
  this.projection = {};
  this.mapContainer = container;
  this.markers = [];
  this.mapWidth = container.clientWidth;
  this.mapHeight = container.clientHeight;
  this.offset = bounds.tileOffset;

  var self = this;
  
  /**
   * Create a Mercator grid from lat/lng
   * @param  {[type]} geo  [description]
   * @param  {[type]} size [description]
   * @return {[type]}   [description]
   */
  function getGridBounds(geo, size) {

    // get the lat / long tile number and then convert that back to bounds?
    // region map bound by the left hand corner
    // http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
    function long2tile(lon,zoom) { return (Math.floor((lon+180)/360*Math.pow(2,zoom))); }
    function lat2tile(lat,zoom)  { return (Math.floor((1-Math.log(Math.tan(lat*Math.PI/180) + 1/Math.cos(lat*Math.PI/180))/Math.PI)/2 *Math.pow(2,zoom))); }
    function tile2long(x,zoom) { return (x/Math.pow(2,zoom)*360-180); }
    function tile2lat(y,zoom) {
      var n=Math.PI-2*Math.PI*y/Math.pow(2,zoom);
      return (180/Math.PI*Math.atan(0.5*(Math.exp(n)-Math.exp(-n))));
    }

    function distance(point1, point2) {
      var earthRadius = 6371;
      var a = 
       0.5 - Math.cos((point2.latitude - point1.latitude) * Math.PI / 180)/2 + 
       Math.cos(point1.latitude * Math.PI / 180) * Math.cos(point2.latitude * Math.PI / 180) * 
       (1 - Math.cos((point2.longitude - point1.longitude) * Math.PI / 180))/2;
      return earthRadius * 2 * Math.asin(Math.sqrt(a));
    }
    
    /**
     * Generate the grid
     * @param  {[type]} tile [description]
     * @param  {[type]} size [description]
     * @return {[type]}   [description]
     */
    function grid (tile, size) {
      var x = long2tile(tile.longitude, tile.zoom),
        y = lat2tile(tile.latitude, tile.zoom);

      return {
        geo : {
          latitude  : tile.latitude,
          longitude : tile.longitude,
          zoom  : tile.zoom,
        },
        width : size.width,
        height  : size.height,
        left  : x,
        right : x + size.width,
        top : y,
        bottom  : y + size.height
      };
    }

    /**
     * Get the bounds of the tile range
     * @param  {[type]} grid [description]
     * @return {[type]}   [description]
     */
    function mercatorBounds(grid) {
      var bounds = {
          leftLongitude : tile2long(grid.left, grid.geo.zoom),
          rightLongitude  : tile2long(grid.right, grid.geo.zoom),// left and right bounds of the box,
          bottomLatitude  : tile2lat(grid.bottom, grid.geo.zoom),
          topLatitude   : tile2lat(grid.top, grid.geo.zoom),
        };
      
      console.info('Mercator Projection >>> ', bounds);
      return bounds;
    }

    self.grid = grid(geo, size);

    

    console.info('Projection Grid >>> ', self.grid);
    self.projection = mercatorBounds(self.grid);
    return self.projection;
  }


  this.bounds = getGridBounds(bounds.geo, bounds.size);
  
  var resizeMap = debounce(function(){
    if(self.markers.length){
      self.mapWidth = self.mapContainer.clientWidth;
      self.mapHeight = self.mapContainer.clientHeight;
      console.info('Map resized');
      self.positionMarkers();
    }
  }, 200, true);
  //reposition the markers when the map changes
  // window.addEventListener('resize', resizeMap);
}

Map.prototype.setOffset = function(position){
  this.offset.x = position.x;
  this.offset.y = position.y;
};

/**
 * remove the markers and listeners
 * @return {[type]} [description]
 */
Map.prototype.destroy = function () {
  if(!this.markers.length) {
    return false;
  }
  this.markers.forEach(function(marker, index){
    marker.elem.parentNode.removeChild(marker.elem);
  });
  this.markers = [];
};

/**
 * Add markers to the map
 * @param {[type]} markers [description]
 * @param {[type]} element [description]
 */
Map.prototype.addMarkers = function (markers, element) {
  var self = this;
  markers.forEach(function(marker, index){
    self.addMarker(marker, element);
  });
};

/**
 * Add markers to the map
 * @param {[type]} markers [description]
 * @param {[type]} element [description]
 */
Map.prototype.addMarker = function (marker, element) {
  if(!marker) {
    return false;
  }
  // create the marker
  var _marker = document.createElement('div');
  _marker.classList.add('marker');
  _marker.setAttribute('id', marker.id);
  // add the icon
  // var _markerIcon = document.createElement('img');
  // _markerIcon.src = MAP_ICON;
  // _markerIcon.setAttribute('alt', marker);
  // _markerIcon.setAttribute('data-marker', marker);
  // _markerIcon.classList.add('marker__icon');
  
  var markerDot = document.createElement('div');
  markerDot.setAttribute('alt', marker);
  markerDot.setAttribute('data-marker', marker);
  markerDot.classList.add('marker__point');

  if(marker.click) {
    _marker.addEventListener('click', function(){
      _marker.classList.toggle('active');
      marker.click.call();
    });
  }

  element.appendChild(_marker);
  element.appendChild(markerDot);
  // _marker.appendChild(_markerIcon);

  var reference = {
    elem : _marker,
    latitude : marker.latitude,
    longitude : marker.longitude
  };

  this.markers.push(reference);
  this.positionMarker(reference);
};

/**
 * Set position of the marker
 * @param  {[type]} marker [description]
 * @return {[type]}   [description]
 */
Map.prototype.positionMarker = function (marker) {
  
  /**
   * Convert lat / lng / zoom to pixel placement
   * http://stackoverflow.com/questions/2103924/mercator-longitude-and-latitude-calculations-to-x-and-y-on-a-cropped-map-of-the
   * @param  {[type]} geo  [description]
   * @param  {[type]} tile [description]
   * @param  {[type]} size [description]
   * @return {[type]}   [description]
   */
  function convertGeoToPixel (geo, tile, size)
  {
    var delta = tile.rightLongitude - tile.leftLongitude,// distance spanned over the total tiles
      bottomLatitudeDegree = tile.bottomLatitude * Math.PI / 180, //
      worldMapWidth = ((size.width / delta) * 360) / (2 * Math.PI), // image width / distance between points * 360 (circle)
      mapOffsetY = (worldMapWidth / 2 * Math.log((1 + Math.sin(bottomLatitudeDegree)) / (1 - Math.sin(bottomLatitudeDegree))));
    
    var lat = geo.latitude * Math.PI / 180;

    return {
      x : (geo.longitude - tile.leftLongitude) * (size.width / delta),
      y : size.height - ((worldMapWidth / 2 * Math.log((1 + Math.sin(lat)) / (1 - Math.sin(lat)))) - mapOffsetY)
    };
  }

  // calculate the marker pixel position based on its GEO
  var position = convertGeoToPixel(marker, this.bounds, {width:this.mapWidth, height:this.mapHeight});
  
  // set the marker top / left % 
  // marker.elem.style.left = (((position.x)-this.offset.x)/this.mapWidth) *100 + '%';
  // marker.elem.style.top = (((position.y)-this.offset.y)/this.mapHeight) *100 + '%';

  marker.elem.style.left =(position.x)-this.offset.x + 'px';
  marker.elem.style.top = (position.y)-this.offset.y + 'px';

  console.info('Marker placed >>> ',marker, position);
};

/**
 * Position multiple markers
 * @return {[type]} [description]
 */
Map.prototype.positionMarkers = function() {
  
  var self = this;

  if(!this.markers.length) {
    console.warn('No Markers');
    return false;
  }

  //markers
  this.markers.forEach(function(marker, index){
    self.positionMarker(marker);
  });
};

Map.prototype.debug = function(element){
  var tiles = [];
  var grid = this.grid;
  var offset = this.offset;
  var tileWidth = 256;
  element.classList.add('debug');

  //duplication
  function long2tile(lon,zoom) { return (Math.floor((lon+180)/360*Math.pow(2,zoom))); }
    function lat2tile(lat,zoom)  { return (Math.floor((1-Math.log(Math.tan(lat*Math.PI/180) + 1/Math.cos(lat*Math.PI/180))/Math.PI)/2 *Math.pow(2,zoom))); }
    function tile2long(x,zoom) { return (x/Math.pow(2,zoom)*360-180); }
    function tile2lat(y,zoom) {
      var n=Math.PI-2*Math.PI*y/Math.pow(2,zoom);
      return (180/Math.PI*Math.atan(0.5*(Math.exp(n)-Math.exp(-n))));
    }

  function createElements(grid, offset) {
    
    // how many tiles
    var tileWidth = 256;//element.clientWidth/grid.width;
    var startTile = {
      x : grid.left,
      y: grid.top
    };
    console.info(grid, startTile);
    for(var y = 0; y < grid.height; y ++)  {
      for(var x = 0; x < grid.width; x ++)  {
        var tile = document.createElement('div');
        tile.style.border = '1px solid rgba(255,0,0,0.3)';
        tile.style.width = tileWidth + 'px';
        tile.style.height = tileWidth + 'px';
        tile.style.position = 'absolute';
        tile.style.top = (tileWidth * y)-offset.y + 'px';
        tile.style.left = (tileWidth * x)-offset.x + 'px';

        tile.style.wordWrap = 'break-word';
        tile.style.fontSize = '10px';
        tile.style.background = 'rgba(255,255,255,0.1)';
        tile.style.padding= '40px';
        tile.innerHTML = '<pre style="color:red;">[' + (startTile.x + x) + ',' + (startTile.y + y) + ']</pre>';
        tiles.push(tile);
      }
    }
  }

  function draw (){
    var resizeTimer;
    if (! tiles.length ) {
      createElements(grid, offset);
      window.addEventListener('resize', function(){
        destroy();
        clearTimeout(resizeTimer);
          resizeTimer = setTimeout(function(){
            draw();
          }, 500);

          DOM.select('header').remove();
          DOM.select('.section__left').remove();
      });
    }
    tiles.forEach(function(tile, index){
      element.appendChild(tile);
    });

      DOM.select('header').remove();
      DOM.select('.section__left').remove();
  }
  function destroy () {
    tiles.forEach(function(tile, index){
      tile.remove();
    });
    tiles = [];
  }
  return draw();
};