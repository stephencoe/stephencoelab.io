define('modules/regions', ['helpers/get', 'modules/sidebar', 'modules/map', 'modules/tracking', 'helpers/router'], function(get, sidebar, map, tracking, router){
  'use strict';


  /**
  * Regions are the top level map, once you click in it becomes experiences...
  * @type {Object}
  */
  
  var curatorColors = {
    'kirsten-alana' : {
      map : '#EEE6D6',
      region : 'rgba(48, 84, 147, 0.8)'
    },
    'rob-lloyd' : {
      map : '#616F5E',
      region: 'rgba(151, 171, 133, 0.5)'
    },
    'rachelle-lucas' : {
      map : '#804749',
      region : 'rgba(231, 155, 154, 0.5)'
    },
    'lars-schneider' : {
      map : '#12394A',
      region : 'rgba(213, 215, 223, 0.8)'
    },
    'markus-radscheit' : {
      map : '#017040',
      region : 'rgba(174, 228, 129, 0.5)'
    },
    'charlie-waite' : {
      map : '#CA7922',
      region : 'rgba(255, 206, 138, 0.8)'
    }
  };

  var regionMercData = {

    'lake-district' : {
      size : {
        width : 6,
        height: 4
      },
      geo : {
        tile : {
          x : 248,
          y : 161
        },
        latitude : 55.379110448010486,
        longitude : -5.625,
        zoom  : 9
      },
      tileOffset : {
        x : -40,
        y : 270
      }
    },

    cornwall : {
      size : {
        width : 6,
        height: 4
      },
      geo : {
        latitude : 51.6180165487737,
        longitude : -7.734375,
        zoom  : 9,
        tile : {
          x : 245,
          y : 170
        },
      },
      tileOffset : {
        x : 80,
        y : 80//-100
      }
    },
    
    'brecon-beacons' : {
      size : {
        width : 6,
        height: 4
      },
      geo : {
        latitude : 52.48278022207821,
        longitude : -6.328125,
        zoom  : 9,
        tile : {
          x : 247,
          y : 168
        },
      },
      tileOffset : {
        x : 60,
        y : -50
      }
    },

    'north-wales' : {
      size : {
        width : 6,
        height: 4
      },
      geo : {
        latitude : 53.74871079689899,
        longitude : -6.328125,
        zoom  : 9,
        tile : {
          x : 247,
          y : 165
        },
      },
      tileOffset : {
        x : -70,
        y : -60
      }
    },

    'scottish-highlands-islands' : {
      size : {
        width : 6,
        height: 4
      },
      geo : {
        latitude : 60.239811169998916,
        longitude : -14.0625,
        zoom  : 7,
        tile : {
          x : 59,
          y : 37
        },
      },
      tileOffset : {
        x : -70,
        y : -50
      }
    },

    //testing

    pembrokeshire : {
      size : {
        width : 6,
        height: 4
      },
      geo : {
        latitude : 52.90890204777026,
        longitude : -7.03125,
        zoom  : 9,
        tile : {
          x : 246,
          y : 167
        },
      },
      tileOffset : {
        x : -110,
        y : 330
      }
    },

    'loch-lomond' : {
      size : {
        width : 4,
        height: 3
      },
      geo : {
        latitude : 56.944974180851595,
        longitude : -6.328125,
        zoom  : 9
      },
      tileOffset : {
        x : -140,
        y : 160
      }
    },

    'yorkshire' : {
      size : {
        width : 6,
        height: 4
      },
      geo : {
        latitude : 54.97761367069626,
        longitude : -4.21875,
        zoom  : 9
      },
      tileOffset : {
        x : 130,
        y : 30
      }
    },


    southdowns : {
      size : {
        width : 4,
        height: 3
      },
      geo : {
        latitude : 51.6180165487737,
        longitude : -2.109375,
        zoom  : 9
      },
      tileOffset : {
        x : -200,
        y : -85
      }
    },

    cotswolds : {
      size : {
        width : 6,
        height: 4
      },
      geo : {
        latitude : 52.48278022207821,
        longitude : -4.921875,
        zoom  : 9
      },
      tileOffset : {
        x : 50,
        y : -80
      }
    },
    
    'new-forest' : {
      size : {
        width : 4,
        height: 3
      },
      geo : {
        latitude : 51.6180165487737,
        longitude : -7.734375,
        zoom  : 9
      },
      tileOffset : {
        x : -200,
        y : -85
      }
    },
    
    'peak-district' : {
      size : {
        width : 6,
        height: 4
      },
      geo : {
        latitude : 53.74871079689899,
        longitude : -3.515625,
        zoom  : 10
      },
      tileOffset : {
        x : 80,
        y : 0
      }
    },

  };

  (function(filter){

    var selectedRegion = null,
      allRegions = {},
      countryMap = DOM.select('.country');

    /**
     * Create an SVG
     * @param  {[type]} path [description]
     * @return {Object}  the svg
     */
    function createSVGObject(path){
      var SVG = document.createElement('object');
      
      SVG.setAttribute('data', path);
      SVG.setAttribute('type', 'image/svg+xml');
      return SVG;
    }

    /**
     * Build the dynamic list of available regions
     * @param  {[type]} pageRegions [description]
     * @return {[type]}             [description]
     */
    function buildSelectOptions(pageRegions){
      pageRegions.forEach(function(regionData, index){
        allRegions[regionData._id] = regionData;
        allRegions[regionData._id].key = regionData.title.replace(/\W+/g, '-').toLowerCase();
        
        //select options
        var li = document.createElement('li'),
          link = document.createElement('a');
        li.classList.add(regionData.key);

        li.addEventListener('mouseover', function(){
          mouseOverRegion(regionData);
        });

        li.addEventListener('mouseout', function(){
          mouseOutRegion(regionData);
        });

        link.setAttribute('href', '#/experiences/' + regionData.key);
        link.textContent = regionData.title;
        li.appendChild(link);
        filter.appendChild(li);
      });

      console.info('Regions loaded >>> ', allRegions);
    }

    function mouseOverRegion(regionData){
      //disabled on mobile
      if(isMobile.any){
        return false;
      }
      countryMap.querySelector('.' + regionData.key).classList.add('hover');
      countryMap.classList.add('hover-state');
      DOM.select('.region__select').querySelector('.' + regionData.key).classList.add('hover');
      countryMap.classList.add('hover-state');
    }

    function mouseOutRegion(regionData){
      //disabled on mobile
      if(isMobile.any){
        return false;
      }
      countryMap.querySelector('.' + regionData.key).classList.remove('hover');
      countryMap.classList.remove('hover-state');
      DOM.select('.region__select').querySelector('.' + regionData.key).classList.remove('hover');
    }


    /**
     * Big clickable region for Desktop
     * @param  {[type]} regionData [description]
     * @return {[type]}            [description]
     */
    function createLargeSvg(regionData){
      var regionMap = document.createElement('div'),
        regionImage = createSVGObject('/images/maps/regions/' + regionData.key + '.svg');

      regionImage.classList.add('region__map');
      regionMap.id = regionData._id;
      regionMap.classList.add('country__region');
      regionMap.classList.add(regionData.key);
      
      regionMap.appendChild(regionImage);

      regionImage.onload = function(){
        var svgDoc = regionImage.getSVGDocument();
        
        [].forEach.call(svgDoc.querySelectorAll('#UK path'), function(elem, index) {
          elem.style.fill = curatorColors[window.page.curator].map;
        });

        [].forEach.call(svgDoc.querySelectorAll('#Region path'), function(elem, index) {
          elem.style.fill = curatorColors[window.page.curator].region;
        });

        [].forEach.call(svgDoc.querySelectorAll('#Names path'), function(elem, index) {
          elem.style.fill = 'white';
        });
      };
      regionData.elem = regionImage;
      DOM.select('.region__container').appendChild(regionMap);
    }

    /**
     * Smaller versions of the SVG used to indicate the country map
     * @param  {[type]} regionData [description]
     * @return {[type]}            [description]
     */
    function createSmallSvg(regionData){
      // add a ghost div as you cant click on an SVG embed directly :(
      var clickRegion = document.createElement('div');
      clickRegion.classList.add('click-region');

      var regionInfo = getHtmlTemplate('region__template');
      regionInfo.querySelector('h2').textContent = regionData.title;
      regionInfo.querySelector('p').textContent = regionData.introduction;
      
      // create the mini regions for the big map
      var countryRegion = createSVGObject('/images/maps/regions/' + regionData.key + '-region.svg'),
        regionContainer = document.createElement('div');

      regionContainer.classList.add(regionData.key);
      regionContainer.classList.add('region__map');
      regionContainer.appendChild(clickRegion);
      regionContainer.setAttribute('data-region',regionData.title);
      regionContainer.appendChild(regionInfo);
      regionContainer.appendChild(countryRegion);
      DOM.select('.country').appendChild(regionContainer);

      //trigger a change for the sidebar
      clickRegion.addEventListener('click', function(){
        if(countryMap.classList.contains('small')) {
          return false;
        }
        window.location.hash = '#/experiences/' + regionData.key;
        tracking.event('Region', 'Show', regionData.title);
      });

      //show the region data when hover
      clickRegion.addEventListener('mouseover', function(){
        mouseOverRegion(regionData);
      });

      clickRegion.addEventListener('mouseout', function(){
        mouseOutRegion(regionData);
      });

      //hide the default styles
      countryRegion.onload = function(){
        var svgDoc = countryRegion.getSVGDocument();

        [].forEach.call(svgDoc.querySelectorAll('#UK'), function(elem, index) {
          elem.style.display = 'none';
        });

        [].forEach.call(svgDoc.querySelectorAll('#Region path'), function(elem, index) {
          elem.style.fill = curatorColors[window.page.curator].region;
        });

        [].forEach.call(svgDoc.querySelectorAll('#Names'), function(elem, index) {
          elem.style.display = 'none';
        });

        [].forEach.call(svgDoc.querySelectorAll('#Roads'), function(elem, index) {
          elem.style.display = 'none';
        });
      };
    }

    /**
     * Create the dynamic SVGs for interactions
     * @return {[type]} [description]
     */
    function buildRegionSvgs(){
      for(var regionKey in allRegions) {
        // add the region to the country map
        createLargeSvg(allRegions[regionKey]);

        // add a ghost div as you cant click on an SVG embed directly
        createSmallSvg(allRegions[regionKey]);
      }
    }

    function resetMap(){
      var activeRegion = DOM.select('.country__region.active');
      if(activeRegion) {
        DOM.select('.country__region.active').classList.remove('active');
        DOM.select('.country.small').classList.remove('small');
      }
      
    }
    
    /**
     * populate the region 
     * @param  {[type]} regionId [description]
     * @return {[type]}          [description]
     */
    function showRegion(regionId) {
      
      experiences.showRegionExperiences(regionId);

      selectedRegion = allRegions[regionId];

      // remove the active region
      if(DOM.select('.country .active')) {
        DOM.select('.country .active').classList.remove('active');  
      }

      //disabled on mobile
      if(!document.getElementById(regionId)) {
        return false;
      }

      document.getElementById(regionId).classList.add('active');

      //shrink the map
      countryMap.classList.add('small');
      countryMap.querySelector('.' + selectedRegion.key).classList.add('active');
      
      if(!allRegions[regionId].markers) {
        // the request happened already, no need to go again
        addRegionMarkers(regionId);
      }
    }

    function addRegionMarkers(regionId){
      //load the experiences for the section (send the page._id)
      get('/api/region-experiences/' + window.page.language + '/' + regionId + '/' + window.page.id, {}, function(experiences){
        allRegions[regionId].markers = [];
        experiences = JSON.parse(experiences);
        
        // loop the experiences and get all the marker points
        experiences.forEach(function(experience, index){
          if(!experience.hasOwnProperty('location') || !experience.location.hasOwnProperty('geo')) {
            return false;
          }

          allRegions[regionId].markers.push({
            id : experience._id,
            latitude : experience.location.geo[1],
            longitude : experience.location.geo[0],
            experience : experience,
            // click : function(){
            //  window.location.hash = '#/experience/' + experience._id;
            // }
          });
        });
        
        // create the region map
        this._map = new Map(selectedRegion.elem, regionMercData[selectedRegion.key]);
        // this._map.setMarkerDimensions(32, 32);
        // this._map.debug(document.getElementById(regionId));

        this._map.addMarkers(allRegions[regionId].markers, document.getElementById(regionId));
      });
    }

    function getHtmlTemplate(id) {
      var wrapper= document.createElement('div');
      wrapper.innerHTML = document.getElementById(id).innerHTML.replace(/(\n|\s\s+)/g, '');
      return wrapper.firstChild;
    }

    var experiences = {
      /**
      * Show the experience Grid
      * @param  {[type]} region [description]
      * @return {[type]}    [description]
      */
      showRegionExperiences : function(regionId){
        

        get('/api/render-experiences/' + window.page.language + '/' + regionId + '/' + window.page.id, {}, function(regionExperiences){
          DOM.select('.sidebar .experience__grid').innerHTML = regionExperiences;

          //create an svg for the top panel 
          var div = document.createElement('div'),
            SVGObject = createSVGObject('/images/maps/regions/' + selectedRegion.key + '.svg');
          div.classList.add('experience__region__map');

          // add the svg on the top of the sidebar for region reference
          SVGObject.onload = function(){
            var svgDoc = SVGObject.getSVGDocument();
            [].forEach.call(svgDoc.querySelectorAll('#UK path'), function(elem, index) {
              elem.style.display = 'none';
            });

            [].forEach.call(svgDoc.querySelectorAll('#Region path'), function(elem, index) {
              elem.style.fill = curatorColors[window.page.curator].map;
            });

            [].forEach.call(svgDoc.querySelectorAll('#Names path'), function(elem, index) {
              elem.style.fill = 'white';
            });
          };
          var spacer = 0;
          DOM.eachElement('.experience__container', function(element, index){
            element.querySelector('img').addEventListener('load', function(){
              setTimeout(function(){element.classList.add('loaded');}, (300 + spacer));
              spacer = spacer + 30;
            });

            element.addEventListener('mouseover', function(){
              var elem = document.getElementById(element.getAttribute('data-id'));
              if(elem) {
                elem.classList.add('hover');
              }
            });
            element.addEventListener('mouseout', function(){
              var elem = document.getElementById(element.getAttribute('data-id'));
              if(elem) {
                elem.classList.remove('hover');
              }
            });

            
          });

          div.appendChild(SVGObject);

          DOM.select('.experience__region').appendChild(div);

          //show the region select when the user closes the sidebar
          DOM.select('.sidebar__close').addEventListener('click', function(){
            resetMap();
            experiences.close();
          });

          DOM.select('main').addEventListener('click', function(){
            resetMap();
            experiences.close();
          });

          tracking.event('Experiences', 'ShowAll', selectedRegion.title);
        });
      },
      /**
       * Show a single experience
       * @param  {[type]} experienceId [description]
       * @return {[type]}              [description]
       */
      open : function(experienceId){
        console.log(experienceId);
        get('/api/experiences/' + window.page.language + '/' + experienceId, {}, function(response){
          
          DOM.select('.sidebar .experience__details').innerHTML = response;
          DOM.select('.sidebar .experience__details').classList.add('experience--shown');
          DOM.select('.sidebar .experience__grid').classList.add('mask');
          
          DOM.select('.experience__back__button').addEventListener('click', function(){
            experiences.close();
            experiences.showRegionExperiences(selectedRegion._id);
            tracking.event('Experiences', 'ShowAll', selectedRegion.title);
          });

          tracking.event('Experiences', 'Show', DOM.select('.experience__details__name h2').textContent);
        });
        
      },

      /**
       * Close single experience
       * @return {[type]} [description]
       */
      close : function(){
        console.log('remove the experience');
        DOM.select('.sidebar .experience__details').innerHTML = '';
        DOM.select('.sidebar .experience__details').classList.remove('experience--shown');
        DOM.select('.sidebar .experience__grid').classList.remove('mask');
      }
    };

    
    if(filter) {
      
      //add the routes
      //Show single experience
      router.add('experience/:id', function (req) {
        var bodyTop = DOM.select('body').style.top;
        window.pageYOffset = DOM.select('.experiences').getBoundingClientRect().top;
        sidebar.show('#experiences');
        experiences.open(req.params.id);
        DOM.select('body').style.top = bodyTop;
      });

      //Show region experiences
      router.add('experiences/:region', function (req) {
        sidebar.show('#experiences');
        for(var id in allRegions) {
          if(allRegions[id].key ===req.params.region) {
            selectedRegion = allRegions[id];
          }
        }
        showRegion(selectedRegion._id);
      });

      //initialise the regions
      get('/api/regions/' + window.page.language + '/' + window.page.id, {}, function(regions){
        regions = JSON.parse(regions);

        if(!regions) {
          console.warn('Unable to load Regions >>> ', regions);
          return false;
        }

        buildSelectOptions(regions);
        
        // disable regions on mobile and IE8
        if(!isMobile.any && document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#Image", "1.1")) {
          buildRegionSvgs();
        }
      });

    }
    
  })(DOM.select( '.region__select ul' ));
});