---
title: Hello World
author: Stephen Coe
date: 2016-12-01
layout: post.pug
shortcodes: true
excerpt: 
---
# Hello World

Isn't that how everything starts?

Every developer has wrote the words "<a href="https://en.wikipedia.org/wiki/%22Hello,_World!%22_program" target="_blank">Hello world</a>" at some point so why not follow a tradition that has been there since way before I was even considered.



