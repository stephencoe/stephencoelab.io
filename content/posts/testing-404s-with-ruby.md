---
title: Testing 404s with ruby
author: Stephen Coe
date: 2017-02-03
layout: post.pug
excerpt: 
tag: snippet, ruby
shortcodes: true
---
# Testing 404s 

Testing for 404s in automation can be done using a headless driver such as poltergiest which can inspect network traffic.

Doing this allows to test againt the status code of an asset and fail should it find any 404s.

{{gist hash=2b9a60569c7c350283df1606e8ee21e3 lang=ruby file=Makefile /}}
